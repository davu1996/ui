@extends('layouts/blankLayout')

@section('title', 'Login Basic - Pages')

@section('page-style')
    <!-- Page -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/css/pages/page-auth.css') }}">
@endsection

@section('content')

    <div class="authentication-wrapper authentication-cover">
        <!-- Logo -->
        <a href="{{ url('/') }}" class="auth-cover-brand d-flex align-items-center gap-2">
            <span class="app-brand-logo demo">@include('_partials.macros', ['height' => 20, 'withbg' => 'fill: #fff;'])</span>
            <span class="app-brand-text demo text-heading fw-semibold">{{ config('variables.templateName') }}</span>
        </a>
        <!-- /Logo -->
        <div class="authentication-inner row m-0">
            <!-- /Left Section -->
            <div class="d-none d-lg-flex col-lg-7 col-xl-8 align-items-center justify-content-center p-5 pb-2">
                <div>
                    <img src="{{ asset('assets/img/\backgrounds/auth-cover-login-illustration-light.png') }}"
                        class="authentication-image-model d-none d-lg-block" alt="auth-model"
                        data-app-light-img="illustrations/auth-cover-login-illustration-light.png"
                        data-app-dark-img="illustrations/auth-cover-login-illustration-dark.png">
                </div>
                <img src="{{ asset('assets/img/illustrations/tree-3.png') }}" alt="tree"
                    class="authentication-image-tree">
                <img src="{{ asset('assets/img/illustrations/auth-basic-mask-light.png') }}"
                    class="scaleX-n1-rtl authentication-image d-none d-lg-block w-75" alt="triangle-bg"
                    data-app-light-img="illustrations/auth-cover-mask-light.png"
                    data-app-dark-img="illustrations/auth-cover-mask-dark.png">
            </div>
            <!-- /Left Section -->

            <!-- Login -->
            <div
                class="d-flex col-12 col-lg-5 col-xl-4 align-items-center authentication-bg position-relative py-sm-5 px-4 py-4">
                <div class="w-px-400 mx-auto pt-5 pt-lg-0">
                    <h4 class="mb-2">{{ config('variables.templateName') }}! 👋</h4>
                    <p class="mb-4">Please sign-in to your account and start the adventure</p>

                    <form id="formAuthentication" class="mb-3 fv-plugins-bootstrap5 fv-plugins-framework"
                        action="{{ url('/') }}" method="GET" novalidate="novalidate">
                        <div class="form-floating form-floating-outline mb-3 fv-plugins-icon-container">
                            <input type="text" class="form-control" id="email" name="email-username"
                                placeholder="Enter your email or username" autofocus="">
                            <label for="email">Email or Username</label>
                            <div
                                class="fv-plugins-message-container fv-plugins-message-container--enabled invalid-feedback">
                            </div>
                        </div>
                        <div class="mb-3 fv-plugins-icon-container">
                            <div class="form-password-toggle">
                                <div class="input-group input-group-merge">
                                    <div class="form-floating form-floating-outline">
                                        <input type="password" id="password" class="form-control" name="password"
                                            placeholder="············" aria-describedby="password">
                                        <label for="password">Password</label>
                                    </div>
                                    <span class="input-group-text cursor-pointer"><i
                                            class="mdi mdi-eye-off-outline"></i></span>
                                </div>
                            </div>
                            <div
                                class="fv-plugins-message-container fv-plugins-message-container--enabled invalid-feedback">
                            </div>
                        </div>
                        <div class="mb-3 d-flex justify-content-between flex-wrap">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="remember-me">
                                <label class="form-check-label me-2" for="remember-me">
                                    Remember Me
                                </label>
                            </div>
                            <a href="{{ url('auth/forgot-password-basic') }}" class="float-end mb-1">
                                <span>Forgot Password?</span>
                            </a>
                        </div>
                        <button class="btn btn-primary d-grid w-100 waves-effect waves-light">
                            Sign in
                        </button>
                        <input type="hidden">
                    </form>

                    <p class="text-center mt-2">
                        <span>New on our platform?</span>
                        <a href="{{ url('auth/register-basic') }}">
                            <span>Create an account</span>
                        </a>
                    </p>

                    <div class="divider my-4">
                        <div class="divider-text">or</div>
                    </div>

                    <div class="d-flex justify-content-center gap-2">
                        <a href="javascript:;"
                            class="btn btn-icon btn-lg rounded-pill btn-text-facebook waves-effect waves-light">
                            <i class="tf-icons mdi mdi-24px mdi-facebook"></i>
                        </a>

                        <a href="javascript:;"
                            class="btn btn-icon btn-lg rounded-pill btn-text-twitter waves-effect waves-light">
                            <i class="tf-icons mdi mdi-24px mdi-twitter"></i>
                        </a>

                        <a href="javascript:;"
                            class="btn btn-icon btn-lg rounded-pill btn-text-github waves-effect waves-light">
                            <i class="tf-icons mdi mdi-24px mdi-github"></i>
                        </a>

                        <a href="javascript:;"
                            class="btn btn-icon btn-lg rounded-pill btn-text-google-plus waves-effect waves-light">
                            <i class="tf-icons mdi mdi-24px mdi-google"></i>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /Login -->
        </div>
    </div>

@endsection
